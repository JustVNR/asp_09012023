﻿using Microsoft.AspNetCore.Html;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace _04_TagHelpers.Helpers
{
    public static class EmployeHtmlHelper
    {
        public static IHtmlContent Employe(this IHtmlHelper htmlhelper, string name, string salary, string type, string email)
        {
            return new HtmlString($"<div class=\"text-success\"><a href=mailto:{email}>{name}</a> is {type} and earn {salary}.</div>");
        }
    }
}
