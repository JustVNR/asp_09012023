﻿using System.ComponentModel;

namespace _04_TagHelpers.Models
{
    public class Employe
    {
        public string Name { get; set; } = String.Empty;

        public double Salary { get; set; }

        [DisplayName("Actif")]
        public bool IsActif { get; set; }

        public string Email { get; set; } = String.Empty;

        public EmployeType Type { get; set; } = EmployeType.DEBUTANT;

        [DisplayName("Department")]
        public int DepartmentId { get; set; }
    }

    public enum EmployeType
    {
        DEBUTANT = 1,
        JUNIOR = 2,
        SENIOR = 3
    }
}
