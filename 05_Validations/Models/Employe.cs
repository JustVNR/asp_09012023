﻿using System.ComponentModel.DataAnnotations;

namespace _05_Validations.Models
{
    public class Employe
    {
        [Required] // champ obligatoire
        [Display(Name = "User Name")]
        public string UserName { get; set; } = String.Empty;

        [Required(ErrorMessage = "Mot de passe obligatoire")] // champ obligatoire
        [Display(Name = "Mot de passe")]
        [DataType(DataType.Password)] // Personnalisation du type de l'input
        public string Password { get; set; } = String.Empty;

        [Required(ErrorMessage = "Date de naissance obligatoire")]
        [Display(Name = "Date de naissance")]
        [DataType(DataType.Date)]
        public DateTime DateNaissance { get; set; }

        [Required(ErrorMessage = "Email obligatoire")]
        [EmailAddress(ErrorMessage = "Email invalide")]
        public string Email { get; set; } = String.Empty;

        [Required(ErrorMessage = "Evaluation obligatoire")]
        [Range(1, 10)]
        public int Evaluation { get; set; }

        [Required(ErrorMessage = "Téléphone obligatoire")]
        [Display(Name = "Téléphone")]
        [DataType(DataType.PhoneNumber)]
        [RegularExpression(@"^(\d{10})$", ErrorMessage = "Le numéro doit être composé de 10 chiffres")]
        public string NumeroTelephone { get; set; } = String.Empty;

        [Required(ErrorMessage = "Commentaire obligatoire")]
        [DataType(DataType.MultilineText)]
        public string Commentaire { get; set; } = String.Empty;

        [Required(ErrorMessage = "Avatar obligatoire")]
        [Display(Name = "Uploder l'avatar")]
        [CustomAvatarValidation]
        public IFormFile? File { get; set; }


    }

    public class CustomAvatarValidationAttribute : ValidationAttribute
    {
        public override bool IsValid(object? value)
        {
            int sizeMax = 1024 * 1024; // 1MB

            string[] allowedExtensions = new string[] { ".jpg", ".jpeg", ".png" };

            if (value is not IFormFile file)
            {
                return false;
            }

            if (file.Length > sizeMax)
            {
                ErrorMessage = $"Fichier trop gros, la taille maximum autorisée est {sizeMax / 1024 / 1024} MB.";

                return false;
            }

            // string extension = file.FileName.Substring(file.FileName.LastIndexOf('.'));
            string extension = file.FileName[file.FileName.LastIndexOf('.')..];

            if (!allowedExtensions.Contains(extension))
            {
                ErrorMessage = $"Type de fichier invalide. Extensions autorisées : {String.Join(", ", allowedExtensions)}";

                return false;
            }

            return true;
        }
    }
}
