﻿using _05_Validations.Models;
using Microsoft.AspNetCore.Mvc;

namespace _05_Validations.Controllers
{
    public class EmployeController : Controller
    {
        private readonly IWebHostEnvironment _webHostEnvironment;

        public EmployeController(IWebHostEnvironment webHostEnvironment)
        {
            _webHostEnvironment = webHostEnvironment;
        }

        public IActionResult FormValidation()
        {
            //return View();
            return View("FormValidationHtmlHelpers");
        }
        
        [ValidateAntiForgeryToken] // Pour prévenir le Cross-site https://fr.wikipedia.org/wiki/Cross-site_scripting
        [HttpPost]
        public async Task<IActionResult> FormValidation(Employe emp)
        {
            if (ModelState.IsValid)
            {
                if (emp.File is not null && emp.File.Length > 0)
                {
                    string uploads = Path.Combine(_webHostEnvironment.WebRootPath, "uploads");
                    string filePath = Path.Combine(uploads, emp.File.FileName);
                    // string filePath = Path.Combine(uploads, Guid.NewGuid().ToString());

                    using (Stream fs = new FileStream(filePath, FileMode.Create))
                    {
                        await emp.File.CopyToAsync(fs);
                    }
                }

                return View("Details", emp);
            }

            return View(emp);
        }
    }
}
