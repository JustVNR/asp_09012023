﻿using _07_Identity.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.AspNetCore.Mvc;

namespace _07_Identity.Filters
{
    public class CustomFilter : ActionFilterAttribute
    {
        private readonly UserManager<ApplicationUser> _userManager;

        public CustomFilter(UserManager<ApplicationUser> userManager)
        {
            _userManager = userManager;
        }

        public override async Task OnActionExecutionAsync(ActionExecutingContext filterContext,
                                         ActionExecutionDelegate next)
        {
            // var isAjax = filterContext.HttpContext.Request.Headers["X-Requested-With"] == "XMLHttpRequest";
            var user = await _userManager.GetUserAsync(filterContext.HttpContext.User);
            var path = filterContext.HttpContext.Request.Path;
            var query = filterContext.HttpContext.Request.QueryString;
            var pathAndQuery = path + query;

            if (user is null)
            {
                filterContext.Result = new RedirectToRouteResult(
                     new RouteValueDictionary
                     {
                        {"Controller", "Account"},
                        {"Action", "Login"},
                        { "ReturnUrl", pathAndQuery }
                     });

            }
            else if (!await _userManager.IsInRoleAsync(user, "admin"))
            {
                filterContext.Result = new RedirectToRouteResult(
                     new RouteValueDictionary
                     {
                        {"Controller", "Account"},
                        {"Action", "AccessDenied"}
                     });

            }
            await base.OnActionExecutionAsync(filterContext, next);
        }
    }
}
