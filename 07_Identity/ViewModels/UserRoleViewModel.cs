﻿namespace _07_Identity.ViewModels
{
    public class UserRoleViewModel
    {
        public string UserId { get; set; } = String.Empty;
        public string UserName { get; set; } = String.Empty;
        public bool IsSelected { get; set; }
    }
}
