﻿using System.ComponentModel.DataAnnotations;

namespace _07_Identity.ViewModels
{
    public class EditUserViewModel
    {
        public string Id { get; set; } = String.Empty;

        [Required]
        public string UserName { get; set; } = String.Empty;

        [Required]
        [EmailAddress]
        public string Email { get; set; } = String.Empty;

        public string Address { get; set; } = String.Empty;

        public List<string> Roles { get; set; } = new();
    }
}
