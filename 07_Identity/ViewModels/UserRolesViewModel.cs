﻿namespace _07_Identity.ViewModels
{
    public class UserRolesViewModel
    {
        public string RoleId { get; set; } = String.Empty;
        public string RoleName { get; set; } = String.Empty;
        public bool IsSelected { get; set; }
    }
}
