﻿using Microsoft.Build.Framework;

namespace _07_Identity.ViewModels
{
    public class CreateRoleViewModel
    {
        [Required]
        public string RoleName { get; set; } = String.Empty;
    }
}
