﻿using System.ComponentModel.DataAnnotations;

namespace _07_Identity.ViewModels
{
    public class EditRoleViewModel
    {
        [Display(Name = "Role Id")]
        public string Id { get; set; } = String.Empty;

        [Required]
        [Display(Name = "Role Name")]
        public string RoleName { get; set; } = String.Empty;

        public List<string> Users { get; set; } = new();
    }
}
