﻿using _06_TP_MVC.Models;

namespace _06_TP_MVC.ViewModels
{
    public class ProductsPagingViewModel : PagingViewModel
    {
        public IList<Product>? Products { get; set; }
    }
}
