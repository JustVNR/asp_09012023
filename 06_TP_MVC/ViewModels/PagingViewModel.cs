﻿namespace _06_TP_MVC.ViewModels
{
    public class PagingViewModel
    {
        public int CurrentPage { get; set; }
        public int PageSize { get; set; }
        public int TotalRecords { get; set; }
    }
}
