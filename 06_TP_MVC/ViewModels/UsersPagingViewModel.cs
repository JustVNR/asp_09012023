﻿using _06_TP_MVC.Models;

namespace _06_TP_MVC.ViewModels
{
    public class UsersPagingViewModel : PagingViewModel
    {
        public IList<User>? Users { get; set; }
    }
}
