﻿using _06_TP_MVC.Models;

namespace _06_TP_MVC.ViewModels
{
    public class ProductsViewModel
    {
        public string Filter { get; set; } = String.Empty;

        public IEnumerable<Product>? Products { get; set; }
    }
}
