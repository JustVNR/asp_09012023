using _06_TP_MVC.DAO;
using _06_TP_MVC.Models;
using _06_TP_MVC.Services;
using _06_TP_MVC.Settings;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using NLog.Extensions.Logging;
using System.Globalization;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Logging.AddNLog(); // Ajoute NLog en tant que provider de log

builder.Services.AddControllersWithViews();

builder.Services.AddDbContext<ApplicationDbContext>(options => options.UseSqlServer(builder.Configuration.GetConnectionString("DefaultConnection")));

// builder.Services.Configure<EmailSettings>(builder.Configuration.GetSection("MailSettings")); // BUG

var mailSettings = builder.Configuration.GetSection("MailSettings");

builder.Services.Add(new ServiceDescriptor(typeof(EmailSettings), 
     c=> new EmailSettings()
     {
         Mail = mailSettings.GetValue<string>("Mail"),
         DisplayName = mailSettings.GetValue<string>("DisplayName"),
         Password = mailSettings.GetValue<string>("Password"),
         Host = mailSettings.GetValue<string>("Host"),
         Port = mailSettings.GetValue<int>("Port")
     }, ServiceLifetime.Transient));

// ProductDAO inject� par d�pendance
builder.Services.AddScoped<IProductDAO, ProductDAO>();

builder.Services.AddScoped<IProductService, ProductService>();
builder.Services.AddScoped<IEmailService, EmailService>();

builder.Services.AddScoped<IUserDAO, UserDAO>();
builder.Services.AddScoped<IUserService, UserService>();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (!app.Environment.IsDevelopment())
{
    app.UseExceptionHandler("/Home/Error");
    // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
    app.UseHsts();
}

app.UseHttpsRedirection();
//app.UseRequestLocalization("fr-FR");
//app.UseRequestLocalization("en-US");

var cultureInfo = new CultureInfo("fr-FR");
cultureInfo.NumberFormat.NumberDecimalSeparator = ".";
CultureInfo.DefaultThreadCurrentCulture = cultureInfo;
CultureInfo.DefaultThreadCurrentUICulture = cultureInfo;


app.UseStaticFiles();

app.UseRouting();

app.UseAuthorization();

app.MapControllerRoute(
    name: "default",
    pattern: "{controller=Home}/{action=Index}/{id?}");

app.Run();
