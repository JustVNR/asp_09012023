﻿using _06_TP_MVC.DAO;
using _06_TP_MVC.Models;
using _06_TP_MVC.ViewModels;

namespace _06_TP_MVC.Services
{
    public class UserService : IUserService
    {
        private readonly IUserDAO _dao;

        public UserService(IUserDAO dao)
        {
            _dao = dao;
        }

        public async Task Create(User user)
        {
            await _dao.Create(user);
        }

        public async Task Delete(int id)
        {
            await _dao.Delete(id);
        }

        public async Task<UsersPagingViewModel> GetAll(int currentPage, int pageSize, string filter)
        {
            return await _dao.GetAll(currentPage, pageSize, filter);
        }

        public async Task<User?> GetById(int id)
        {
            return await _dao.GetById(id);
        }

        public async Task Update(User user)
        {
            await _dao.Update(user);
        }
    }
}
