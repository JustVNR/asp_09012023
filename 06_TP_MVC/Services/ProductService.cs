﻿using _06_TP_MVC.DAO;
using _06_TP_MVC.Models;
using _06_TP_MVC.ViewModels;

namespace _06_TP_MVC.Services
{
    public class ProductService : IProductService
    {
        private readonly IProductDAO _dao;
        // private readonly IProductService _service;

        public ProductService(IProductDAO dao)
        {
            _dao = dao;
        }

        public async Task Create(Product product)
        {
            await _dao.Create(product);
        }

        public async Task Delete(int id)
        {
            await _dao.Delete(id);
        }

        public async Task<List<Product>> GetAll(string? description = null)
        {
            return await _dao.GetAll(description);
        }

        public async Task<ProductsPagingViewModel> GetAll(int currentPage, int pageSize, string filter)
        {
            return await _dao.GetAll(currentPage, pageSize, filter);
        }

        public async Task<Product?> GetById(int id)
        {
            return await _dao.GetById(id);
        }

        public async Task Update(Product product)
        {
            await _dao.Update(product);
        }
    }
}
