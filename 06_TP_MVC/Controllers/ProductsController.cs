﻿using _06_TP_MVC.DAO;
using _06_TP_MVC.Models;
using _06_TP_MVC.Services;
using _06_TP_MVC.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace _06_TP_MVC.Controllers
{
    public class ProductsController : Controller
    {
        private readonly IProductService _service;

        public ProductsController(IProductService service)
        {
            _service = service;
        }

        // GET: Products
        public async Task<IActionResult> Index()
        {
            return View(await _service.GetAll());
        }

        // GET: Products/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var product = await _service.GetById((int)id);

            if (product == null)
            {
                return NotFound();
            }

            return View(product);
        }

        // GET: Products/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Products/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Description,Prix")] Product product)
        {
            if (ModelState.IsValid)
            {
                await _service.Create(product);
                return RedirectToAction(nameof(Index));
            }
            return View(product);
        }

        // GET: Products/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var product = await _service.GetById((int)id);
            if (product == null)
            {
                return NotFound();
            }
            return View(product);
        }

        // POST: Products/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Description,Prix")] Product product)
        {
            if (id != product.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    await _service.Update(product);
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!ProductExists(product.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(product);
        }

        // GET: Products/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var product = await _service.GetById((int)id);
            if (product == null)
            {
                return NotFound();
            }

            return View(product);
        }

        // POST: Products/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var product = await _service.GetById(id);

            if (product != null)
            {
                await _service.Delete(id);
            }

            return RedirectToAction(nameof(Index));
        }

        private bool ProductExists(int id)
        {
            return _service.GetById(id) is not null;
        }

        // ----------------------------------------------------------------
        // ---------------------- AJAX FILTERED ---------------------------
        // ----------------------------------------------------------------

        // GET: Products
        public async Task<IActionResult> IndexFiltered()
        {
            return View(await _service.GetAll());
        }

        public async Task<IActionResult> _GetByDescription(string desc)
        {
            return PartialView("_ProductsPage", await _service.GetAll(desc));
        }

        // ----------------------------------------------------------------
        // --------------- AJAX FILTERED UNOBSTUSIVE ----------------------
        // ----------------------------------------------------------------

        // GET: Products
        public async Task<IActionResult> IndexFilteredUnobstrusive()
        {
            ProductsViewModel vm = new()
            {
                Products = await _service.GetAll()
            };

            return View(vm);
        }

        public async Task<IActionResult> _IndexPartial(ProductsViewModel vm)
        {
            return PartialView("_ProductsPage", await _service.GetAll(vm.Filter));
        }

        // ----------------------------------------------------------------
        // ------------------------ PAGINATION ----------------------------
        // ----------------------------------------------------------------

        public async Task<IActionResult> IndexPagined([FromQuery] int p = 1, [FromQuery] int s = 1, [FromQuery] string q = "")
        {
            return View(await _service.GetAll(p, s, q));
        }
    }
}
