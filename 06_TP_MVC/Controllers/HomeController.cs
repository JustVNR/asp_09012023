﻿using _06_TP_MVC.Services;
using _06_TP_MVC.ViewModels;
using LazZiya.TagHelpers.Alerts;
using Microsoft.AspNetCore.Mvc;
using System.Diagnostics;

namespace _06_TP_MVC.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly IEmailService _mail;

        public HomeController(ILogger<HomeController> logger, IEmailService mail)
        {
            _logger = logger;
            _mail = mail;
        }

        public IActionResult Index()
        {
            // _logger.LogError("TEST NLOG");

            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        [HttpGet]
        public IActionResult Contact()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Contact(EmailViewModel message)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    _mail.SendEmailAsync(message);

                    /* Pour utiliser toastr il faut : 
                     * - créer une vue partielle (_Notificaiton)
                     * - la rajouter à _Layout
                     * - rajouter le css de toastr dans le layout
                     * - Enoyer des TempData["success"] ou TempData["error"]...
                     */

                    TempData["success"] = "Message envoyé avec succès"; // Notification toastr
                    TempData.Success("Message envoyé avec succès"); // Alert TagHelper LaZzyia

                    return View(nameof(Index));
                }
                catch (Exception e)
                {
                    TempData["error"] = "Message non envoyé"; // Notification toastr
                    TempData.Danger("Message non envoyé"); // Alert TagHelper LaZzyia

                    if (e.Message is not null)
                    {
                        _logger.LogError(e.Message);
                    }
                }
            }
            return View(message);
        }
    }
}