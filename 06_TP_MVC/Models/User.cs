﻿
using System.ComponentModel.DataAnnotations;

namespace _06_TP_MVC.Models
{
    public class User
    {
        [Key]
        public int Id { get; set; }

        [Required]
        [EmailAddress]
        public string Email { get; set; } = String.Empty;

        [Required]
        [DataType(DataType.Password)]
        public string Password { get; set; } = String.Empty;

        [Required]
        [Display(Name ="Is Admin")]
        public bool IsAdmin { get; set; }

        public User()
        {

        }

        public User(int id, string email, string password, bool isAdmin)
        {
            Id = id;
            Email = email;
            Password = password;
            IsAdmin = isAdmin;
        }
    }
}
