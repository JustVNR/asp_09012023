﻿using _06_TP_MVC.Models;
using _06_TP_MVC.ViewModels;

namespace _06_TP_MVC.DAO
{
    public interface IUserDAO
    {
        Task<UsersPagingViewModel> GetAll(int currentPage, int pageSize, string filter);
        Task<User?> GetById(int id);
        Task Create(User user);
        Task Update(User user);
        Task Delete(int id);
    }
}
