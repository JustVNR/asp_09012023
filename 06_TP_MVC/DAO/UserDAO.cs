﻿using _06_TP_MVC.Models;
using _06_TP_MVC.ViewModels;
using Microsoft.EntityFrameworkCore;

namespace _06_TP_MVC.DAO
{
    public class UserDAO : IUserDAO
    {
        private readonly ApplicationDbContext _db;

        public UserDAO(ApplicationDbContext db)
        {
            _db = db;
        }

        public async Task Create(User user)
        {
            _db.Add(user);
            await _db.SaveChangesAsync();
        }

        public async Task Delete(int id)
        {
            User? user = await _db.Users.FindAsync(id);

            if (user is not null)
            {
                _db.Users.Remove(user);
                await _db.SaveChangesAsync();
            }
        }

        public async Task<UsersPagingViewModel> GetAll(int currentPage, int pageSize, string filter)
        {
            filter ??= String.Empty;

            UsersPagingViewModel model = new()
            {
                CurrentPage = currentPage,
                PageSize = pageSize,
                TotalRecords = await _db.Users.AsNoTracking().Where(x => x.Email.Contains(filter)).CountAsync(),
                Users = await _db.Users.AsNoTracking()
                                        .Where(x => x.Email.Contains(filter))
                                        .Skip((currentPage - 1) * pageSize)
                                        .Take(pageSize)
                                        .ToListAsync()
            };

            return model;
        }

        public async Task<User?> GetById(int id)
        {
            return await _db.Users.FindAsync(id);
        }

        public async Task Update(User user)
        {
            _db.Update(user);
            await _db.SaveChangesAsync();
        }
    }
}
