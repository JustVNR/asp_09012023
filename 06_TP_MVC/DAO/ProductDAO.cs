﻿using _06_TP_MVC.Models;
using _06_TP_MVC.ViewModels;
using Microsoft.EntityFrameworkCore;

namespace _06_TP_MVC.DAO
{
    public class ProductDAO : IProductDAO
    {
        private readonly ApplicationDbContext _db;

        public ProductDAO(ApplicationDbContext db)
        {
            _db = db;
        }

        public async Task Create(Product product)
        {
            _db.Add(product);
            await _db.SaveChangesAsync();
        }

        public async Task Delete(int id)
        {
            Product? product = await _db.Products.FindAsync(id);

            if (product is not null)
            {
                _db.Products.Remove(product);
                await _db.SaveChangesAsync();
            }
        }

        public async Task<List<Product>> GetAll(string? description)
        {
            if (description is null)
            {
                return await _db.Products.ToListAsync();
            }

            return await _db.Products.Where(x => x.Description.Contains(description)).ToListAsync();
        }

        public async Task<ProductsPagingViewModel> GetAll(int currentPage, int pageSize, string filter)
        {
            filter ??= String.Empty;

            ProductsPagingViewModel model = new()
            {
                CurrentPage = currentPage,
                PageSize = pageSize,
                TotalRecords = await _db.Products.AsNoTracking().Where(x => x.Description.Contains(filter)).CountAsync(),
                Products = await _db.Products.AsNoTracking()
                                        .Where(x => x.Description.Contains(filter))
                                        .Skip((currentPage - 1) * pageSize)
                                        .Take(pageSize)
                                        .ToListAsync()
            };

            return model;
        }

        public async Task<Product?> GetById(int id)
        {
            return await _db.Products.FindAsync(id);
            //return await _db.Products.FirstOrDefaultAsync(p => p.Id == id);
        }

        public async Task Update(Product product)
        {
            Product? p = await _db.Products.FindAsync(product.Id);
            // Product? p = await _db.Products.AsNoTracking().FirstOrDefaultAsync(x => x.Id == product.Id);

            if (p is not null)
            {
                //_db.Entry(p).State = EntityState.Detached;

                /*_db.Update(product);
                await _db.SaveChangesAsync();*/

                p.Description = product.Description;
                p.Prix = product.Prix;

                _db.Update(p);
                await _db.SaveChangesAsync();
            }

            /*if (await _db.Products.FindAsync(product.Id) is not null)
            {
                _db.Update(product);
                await _db.SaveChangesAsync();
            }*/
        }
    }
}
