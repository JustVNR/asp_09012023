﻿namespace _02_PassingDataToView.Models
{
    public class Personnage
    {
        public int Id { get; set; }
        public string Nom { get; set; } = String.Empty;
        public string Prenom { get; set; } = String.Empty;
    }
}
