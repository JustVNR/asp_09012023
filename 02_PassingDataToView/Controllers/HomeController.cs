﻿using _02_PassingDataToView.Models;
using Microsoft.AspNetCore.Mvc;
using System.Diagnostics;

namespace _02_PassingDataToView.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;

        public HomeController(ILogger<HomeController> logger)
        {
            _logger = logger;
        }

        public IActionResult Index()
        {
            // Après être passé par la vue 'privacy' sans y avoir été lue, on peut récupérer la donnée stockée dans un TempData
            if (TempData.ContainsKey("tmpDataFromAction"))
            {
                ViewBag.tmpDataFromAction = TempData["tmpDataFromAction"]?.ToString();
            }

            // On récupère les données passé depuis la vue 'privacy' via un tempData
            if (TempData.ContainsKey("tmpDataFromView"))
            {
                ViewBag.tmpDataFromView = TempData["tmpDataFromView"]?.ToString();
            }

            // récupération d'un cookie depuis la requête
            ViewBag.myCookie = Request.Cookies["key"];

            // -- Création d'un cookie
            CookieOptions options = new()
            {
                Expires = DateTime.Now.AddSeconds(10)
            };

            Response.Cookies.Append("key", "cookie miam miam", options);

            Personnage p = new () { Prenom = "Riri", Nom = "Duck" };

            // La Vue "index" prend en paramètre un objet fortement typé de type "Personnage"
            return View(p);
        }

        public IActionResult Privacy()
        {
            /*
             * ViewBag permet de transmettre des données du controleur à la vue.
             * Ces données sont tréansmises en tant que propriétés de l'objet ViewBag.
             * La portée du ViewBag est limitée à la requête actuelle : sa valeur est réinitilisée à null une fois transmis à la vue
             */
            ViewBag.Message = "your application privacy from Viewbag";

            /* ViewData est un objet de type dictionnaire permettant de transmettre des données du controleur à la vue
             * Ces données sont transférées sous forme de paires clé/valeur
             * La portée du ViewData est limitée à la requête actuelle : sa valeur est réinitilisée à null une fois transmis à la vue
             */
            ViewData["Message"] = "your application privacy from ViewData";


            IList<string> list = new List<string>
            {
                "riri",
                "fifi",
                "loulou"
            };

            ViewData["stringlist"] = list;

            // Attention ViewBag.Message <=> ViewData["Message"]
            // Une propriété du ViewBag est reconnues en tant que clé du ViewData et réciproquement

            /*
             * TempData peut être utilisé pour transférer des données :
             * - d'une vue à un contolleur
             * - d'un controlleur à une vue
             * - d'une méthode d'Action à une autre méthodes d'Action dans le même controlleur ou dansun controleur différent
             * 
             * TemData conserve les données tant qu'elles n'ont pas été lues
             */
            TempData["tmpDataFromAction"] = "Data from Action 'privacy' in 'HomeController' with TempData";

            // Session : 
            // - Objet de type dictionnaire
            // - Accessibe par l'ensemble des contrôleurs et des vues
            // - expire par défaut au bout de 20min

            // Attention : il faut rajouter builder.Services.AddSession(); dans program.cs
            HttpContext.Session.SetString("user_name", "riri");
            HttpContext.Session.SetInt32("user_id", 12);

            HttpContext.Session.Remove("user_id");
            HttpContext.Session.Clear();

            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}