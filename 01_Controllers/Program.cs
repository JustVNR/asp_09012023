
// Cr�ation du WebApplicationBuilder
var builder = WebApplication.CreateBuilder(args);

// ---------------------------------------------------
// ----- Ajout de services � l'application------------
// ---------------------------------------------------

// On s'abonne ici � l'ensemble des services dont aura besoin l'application

// Add services to the container.
builder.Services.AddControllersWithViews();

// on s'abonne au service de mise en cache
builder.Services.AddResponseCaching(options =>
{
    options.MaximumBodySize = 1024 * 1024;
    options.UseCaseSensitivePaths = true;
});

// ---------------------------------------------------
// ------------------ Build application --------------
// ---------------------------------------------------

var app = builder.Build();


// ----------------------------------------------------------------------------
// --- Confuguration du pipeline pour construire la r�ponse � la requ�te ------
// ----------------------------------------------------------------------------

// Le pipeline p�cifie la mani�re dont kl'application doit traiter/r�pondre � une requ�te
// Quand l'application re�oit une requ�te du client, cette derni�re va all�/retour � travers le pipeline
// Ce pipeline est constitu� d'un ensemble de middlewares

// Chacun de ces middlewares permet d'adresser un point technique li� � la req�te : 

// - la gestion des erreurs
// - la gestion des cookies
// - l'autehtification
// - le routage
// - la mise en cache...

// Configure the HTTP request pipeline.
if (!app.Environment.IsDevelopment())
{
    // Si pas en environnement de d�veloppement, on redirige vers l'action 'Error' du controller 'Home' en cas d'exception
    app.UseExceptionHandler("/Home/Error");

    // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
    app.UseHsts(); // POur inciter l'utilisateur � utiliser https
}

app.UseHttpsRedirection();
app.UseStaticFiles();

app.UseRouting();

app.UseAuthorization();

/*app.MapControllerRoute(
    name: "default",
    pattern: "{action=Privacy}/{controller=Home}/{id?}");*/

app.MapControllerRoute(
    name: "default",
    pattern: "{controller=Home}/{action=Index}/{id?}");

/*app.MapControllerRoute(
    name: "default",
    pattern: "{controller=new}/{action=ActionReturnView}/{id?}");*/

// Middleware de mise en cache
app.UseResponseCaching();

app.Use(async (context, next) =>
{
    context.Response.GetTypedHeaders().CacheControl =
        new Microsoft.Net.Http.Headers.CacheControlHeaderValue()
        {
            Public = true,
            MaxAge = TimeSpan.FromSeconds(10)
        };
    context.Response.Headers[Microsoft.Net.Http.Headers.HeaderNames.Vary] =
        new string[] { "Accept-Encoding" };

    await next(context);
});

// -------------------------------------------
// ---------------- Run Application ----------
// -------------------------------------------

app.Run();
