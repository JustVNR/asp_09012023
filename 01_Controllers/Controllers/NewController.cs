﻿using Microsoft.AspNetCore.Mvc;

namespace _01_Controllers.Controllers
{
    //[Route("new_route")]
    public class NewController : Controller
    {
        private readonly IWebHostEnvironment _webHostEnvironment;

        public NewController(IWebHostEnvironment webHostEnvironment)
        {
            _webHostEnvironment = webHostEnvironment;
        }

        /* public IActionResult ActionReturnView()
         {
             return View(); // retourne une vue qui porte le même nom que l'action courante dans le controller courant
         }*/

        // /new/ActionReturnString
        // /new/ActionReturnString/2
        // /new/ActionReturnString/2?firstName=riri&lastName=duck
        //[Route("new_action/{id?}")] // /new_route/new_action
        public string ActionReturnString(int? id)
        {
            string first = HttpContext.Request.Query["firstName"].ToString();
            string last = HttpContext.Request.Query["lastName"].ToString();

            return $"Hello from Action 'ActionReturnString' of Controller 'new' with  query string lastName = {last} and firstName = {first} and id={id}";
        }

        // /new/ActionReturnView
        public ViewResult ActionReturnView()
        {
            return View(); // retourne une vue qui porte le même nom que l'action courante dans le controller courant
        }

        // /new/ActionReturnSpecificView
        public ViewResult ActionReturnSpecificView()
        {
            return View("SpecificView"); // retourne une vue qui porte nom "SpecificView" que l'action courante dans le controller courant
        }

        // /new/ActionReturnRedirecToAction
        public ActionResult ActionReturnRedirecToAction()
        {
            return RedirectToAction("ActionReturnString");
        }

        // /new/ActionReturnRedirecToActionWithParameters
        public ActionResult ActionReturnRedirecToActionWithParameters()
        {
            return RedirectToAction("ActionReturnString", new {id = 99, firstName="loulou"});
        }

        // /new/ActionReturnRedirectToRoute
        public ActionResult ActionReturnRedirectToRoute()
        {
            return RedirectToRoute(new { controller = "home", action = "privacy"});
        }

        // /new/ActionReturnJson
        public ActionResult ActionReturnJson()
        {
            return Json("{key:value, key2:{key2:value3}");
        }

        // /new/ActionReturnContent
        public ActionResult ActionReturnContent()
        {
            return Content("<div>Contenu de mon Content depuis ActionReturnContent</div>", "text/html");
        }

        // /new/ActionReturnJavascript
        public ActionResult ActionReturnJavascript()
        {
            return Content("<script>alert(\"return javascript\")</script>", "text/html");
        }

        // /new/ActionReturnStatusCode
        public ActionResult ActionReturnStatusCode()
        {
            return StatusCode(StatusCodes.Status400BadRequest, "Mauvaise requête");
        }

        // /new/ActionReturnFile
        /* public FileResult ActionReturnFile()
         {
             string fileName = "site.css";

             // _webHostEnvironment injecté via constructeur
             string webRootPath = Path.Combine(_webHostEnvironment.WebRootPath, "css/");  ;

             string path = webRootPath + fileName;

             byte[] bytes = System.IO.File.ReadAllBytes(path);

             return File(bytes, "application/octet-stream", fileName);
         }*/

        // /new/ActionReturnFile ATTENTION !!!!!!!! le async est occulté par le MapControllerRoute
        public async Task<FileResult> ActionReturnFileAsync() // 
        {
            string fileName = "site.css";

            // _webHostEnvironment injecté via constructeur
            string webRootPath = Path.Combine(_webHostEnvironment.WebRootPath, "css/"); ;

            string path = webRootPath + fileName;

            // byte[] bytes = System.IO.File.ReadAllBytes(path);
            byte[] bytes = await System.IO.File.ReadAllBytesAsync(path);

            return File(bytes, "application/octet-stream", fileName);
        }
    }
}
