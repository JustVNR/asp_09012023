﻿using _01_Controllers.Models;
using Microsoft.AspNetCore.Mvc;
using System.Diagnostics;

namespace _01_Controllers.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;

        private readonly IConfiguration _config;

        // Injection de dépendance par le constructeur de '_logger' et '_config'
        public HomeController(ILogger<HomeController> logger, IConfiguration config)
        {
            _logger = logger; // _logger récupéré par injection de dépendance
            _config = config;
        }

        // ------------------ Actions ------------------------

        [ResponseCache(Duration = 5)]
        [HttpGet]
        public IActionResult Index()
        {
            _logger.LogTrace("Trace Log");
            _logger.LogDebug("Debug Log");
            _logger.LogInformation("Info Log");
            _logger.LogWarning("Warning Log");
            _logger.LogError("Error Log");
            _logger.LogCritical("Error Log");

            // return View();
            return View("Index", _config.GetValue<string>("testSettings"));
        }

        public IActionResult Privacy()
        {
            _logger.LogTrace("Trace Log from Action Privacy");
            _logger.LogDebug("Debug Log from Action Privacy");
            _logger.LogInformation("Info Log from Action Privacy");
            _logger.LogWarning("Warning Log from Action Privacy");
            _logger.LogError("Error Log from Action Privacy");
            _logger.LogCritical("Error Log from Action Privacy");

            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}